//"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

function problem1(inventory,n){
    let count = 0;
    let array = [];
    let result;
    for(let i = 0;i<inventory.length;i++){
        if(inventory[i].id == n){
            result = "car 33 is a "+inventory[i].car_year+" "+inventory[i].car_make+" "+inventory[i].car_model;
            count += 1;
        }
    }
    if(count>0){
        return result;
    }
    else{
        return array;
    }
        
}
module.exports.problem1=problem1;    