// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const inventory = require("../inventory");
const problem5 = require("../problem5").problem5;
const problem4 = require("../problem4").problem4;


const result = problem5(problem4(inventory));
console.log(result);