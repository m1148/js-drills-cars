// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of: 
const inventory = require("../inventory");
const problem1 = require("../problem1");

const result = problem1.problem1(inventory,33);
console.log(result);