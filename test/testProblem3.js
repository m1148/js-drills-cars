// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

const inventory = require("../inventory");
const problem3 = require("../problem3").problem3;

const result = problem3(inventory);
console.log(result);