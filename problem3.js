function problem3(inventory){
    let count  = 0;
   for(let i = 0;i<inventory.length-1;i++){
       for(let j = i+1;j<inventory.length;j++){
           if(inventory[i].car_model.localeCompare(inventory[j].car_model)>0){
               temp = inventory[i];
               inventory[i] = inventory[j];
               inventory[j] = temp;
               count += 1;
           }
       }
   }
   let result = [];
   if(count >0){
       result = [inventory.length];
        for(let i = 0;i<inventory.length;i++){
                result[i] = inventory[i];
        }
    } 
   return result;
}
module.exports.problem3=problem3;